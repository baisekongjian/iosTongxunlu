//
//  JKContactModel.h
//  ios通讯录(9.0)
//
//  Created by apple on 16/9/11.
//  Copyright © 2016年 LWB. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JKContactModel : NSObject
@property (nonatomic,copy)NSString *name;
@property (nonatomic,copy)NSString *phone;

@end
