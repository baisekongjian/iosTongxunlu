//
//  EditViewController.h
//  ios通讯录(9.0)
//
//  Created by apple on 16/9/11.
//  Copyright © 2016年 LWB. All rights reserved.
//

#import <UIKit/UIKit.h>
@class JKContactModel,EditViewController;
@protocol EditViewControllerDelegate <NSObject>

@optional
-(void)enditViewController:(EditViewController *)editVc didSaveContact:(JKContactModel *)model;

@end
@interface EditViewController : UIViewController
@property (nonatomic,assign) id<EditViewControllerDelegate> delegate;
@property (nonatomic,strong)JKContactModel *contactModel;

@end
