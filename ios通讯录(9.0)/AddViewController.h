//
//  AddViewController.h
//  ios通讯录(9.0)
//
//  Created by apple on 16/9/11.
//  Copyright © 2016年 LWB. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AddViewController,JKContactModel;
@protocol AddViewControllerDelegate <NSObject>

@optional
-(void)addContact:(AddViewController *)addVc didAddContact:(JKContactModel *)contact;

@end
@interface AddViewController : UIViewController

@property (nonatomic,assign)id<AddViewControllerDelegate> delegate;

@end
