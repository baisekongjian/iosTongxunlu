//
//  AddViewController.m
//  ios通讯录(9.0)
//
//  Created by apple on 16/9/11.
//  Copyright © 2016年 LWB. All rights reserved.
//

#import "AddViewController.h"
#import "JKContactModel.h"
@interface AddViewController ()

- (IBAction)backAction:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UITextField *phoneField;
@property (weak, nonatomic) IBOutlet UIButton *addBtn;
- (IBAction)AddAction;
@end

@implementation AddViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //添加观察者
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(textChange) name:UITextFieldTextDidChangeNotification object:self.nameField];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(textChange) name:UITextFieldTextDidChangeNotification object:self.phoneField];
}
-(void)textChange{
    //修改按钮的点击状态
    self.addBtn.enabled=(self.nameField.text.length&&self.phoneField.text.length);
}

//让姓名文本框成为第一响应者
-(void)viewDidAppear:(BOOL)animated{
    [super didReceiveMemoryWarning];
    //让姓名文本框成为第一响应者（叫出键盘）
    [self.nameField becomeFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
//关闭键盘
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.nameField resignFirstResponder];
    [self.phoneField resignFirstResponder];
}
- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:NO];
}
//添加数据
- (IBAction)AddAction {
    //关闭当前视图控制器
    [self.navigationController popViewControllerAnimated:YES];
    //代理传值
    if ([self.delegate respondsToSelector:@selector(addContact:didAddContact:)]) {
        JKContactModel *contactModel = [[JKContactModel alloc]init];
        contactModel.name = self.nameField.text;
        contactModel.phone= self.phoneField.text;
        [self.delegate addContact:self didAddContact:contactModel];
    }
}
@end
