//
//  JKContactModel.m
//  ios通讯录(9.0)
//
//  Created by apple on 16/9/11.
//  Copyright © 2016年 LWB. All rights reserved.
//

#import "JKContactModel.h"

@implementation JKContactModel
/*
 将某个文件写入文件时候会调用
 在这个方法中说清楚哪些属性需要存储
 */

-(void)encodeWithCoder:(NSCoder *)encoder{
    [encoder encodeObject:self.name forKey:@"name"];
    [encoder encodeObject:self.phone forKey:@"phone"];
}

/*
 解析对象会调用这个方法
 需要解析哪些属性
 */
-(id)initWithCoder:(NSCoder *)decoder{
    if (self = [super init]) {
        self.name = [decoder decodeObjectForKey:@"name"];
        self.phone = [decoder decodeObjectForKey:@"phone"];
    }
    return self;
}
@end
