//
//  main.m
//  ios通讯录(9.0)
//
//  Created by apple on 16/9/11.
//  Copyright © 2016年 LWB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
